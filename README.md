# torrserver

## Install

```
$ git clone https://gitlab.com/argo-uln/torrserver.git
```

## Run

```
$ cd torrserver
$ docker compose up -d
```

After open browser link http://127.0.0.1:8090 or external ip your docker host.
